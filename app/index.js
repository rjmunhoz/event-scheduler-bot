'use strict'

const database = require('./database')
const botify = require('@rjmunhoz/botify')
const middlewares = require('./middlewares')

const EventService = require('./services/event')

module.exports = botify((app, config) => {
  const { repositories, storages } = database.factory(config.database)

  const services = {
    events: new EventService(repositories.event, storages.event)
  }

  app.use(middlewares.stage.factory(services))

  app.command('create', ctx => ctx.scene.enter('event-create'))
})
