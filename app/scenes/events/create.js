'use strict'

const moment = require('moment')
const WizardScene = require('telegraf/scenes/wizard')

const factory = (service) => {
  const steps = [(ctx) => {
    ctx.reply('Okay, give me the name of your event')
    ctx.wizard.next()
  }, (ctx) => {
    ctx.scene.state.name = ctx.message.text
    ctx.replyWithMarkdown('Ok, now give me the date using the `YYYY-MM-DD` format')
    ctx.wizard.next()
  }, async (ctx) => {
    const date = moment(ctx.message.text, 'YYYY-MM-DD')

    if (!date.isValid()) {
      ctx.replyWithMarkdown('Oops. This is not a valid date! Please, try again using the `YYYY-MM-DD` format!')
      return
    }

    ctx.scene.state.date = date.toDate()
    ctx.reply(`OK, creating event ${ctx.scene.state.name} at ${date.format('DD/MM/YYYY')}`)
    
    try {
      const { _id } = await service.create(ctx.scene.state)

      ctx.replyWithMarkdown(`Ok, event created. ID: \`${_id}\``)
    } catch (err) {
      ctx.replyWithMarkdown(`Error creating event: \`\`\`\n${err.message}\n\`\`\``)
    }    

    ctx.scene.leave()
  }]

  return new WizardScene('event-create', ...steps)
}

module.exports = { factory }
