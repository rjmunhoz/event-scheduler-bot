'use strict'

const scenes = require('../scenes')
const Stage = require('telegraf/stage')

const factory = (services) => {
  const stage = new Stage()

  stage.register(scenes.events.create.factory(services.events))

  return stage.middleware()
}

module.exports = { factory }
