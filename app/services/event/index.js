'use strict'

const Error = require('./errors/error')
const NotFoundError = require('./errors/not-found-error')
const InvalidDateError = require('./errors/invalid-date-error')

const moment = require('moment')

class EventService {
  constructor (repository, storage) {
    this.$repository = repository
    this.$storage = storage
  }

  async find (id) {
    const event = await this.$repository.find(id)

    if (!event) {
      throw new NotFoundError(id)
    }

    return event
  }

  async findByDate (date) {
    if (!moment(date).isValid()) {
      throw new InvalidDateError(date)
    }

    return this.$repository.findByDate(date)
  }

  async findByOwner (ownerId) {
    return this.$repository.findByOwner(ownerId)
  }

  async create (event) {
    return this.$storage.create(event)
  }

  async update (id, { name, date }) {
    await this.$storage.update(id, { name, date })
  }
}
