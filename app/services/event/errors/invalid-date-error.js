'use strict'

const Error = require('./error')
const { format } = require('util')

const MESSAGE = '"%s" is not a valid date'

class InvalidDateError {
  constructor (date) {
    this.message = format(this.message, date)
  }
}

module.exports = InvalidDateError
