'use strict'

const Error = require('./error')
const { format } = require('util')

const MESSAGE = 'event "%s" was not found'

class NotFoundError extends Error {
  constructor (id) {
    this.message = format(MESSAGE, id)
  }
}

module.exports = NotFoundError
