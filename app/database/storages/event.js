'use strict'

class ExamStorage { 
  constructor (model) {
    this.$model = model
  }

  async create (params) {
    return this.$model.create(params)
                      .then(document => document.toObject())
  }

  async update (id, params) {
    await this.$model.update({ _id: id }, { $set: { ...params } })
  }

  async delete (id) {
    await this.$model.delete(id)
  }
}

module.exports = ExamStorage
