'use strict'

const { Schema } = require('mongoose')

const properties = {
  _id: {
    type: Number,
    required: true
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: false,
    default: () => null
  },
  username: {
    type: String,
    required: false,
    default: () => null
  }
}

const options = {
  id: false,
  safe: true,
  _id: false,
  strict: true,
  versionKey: false
}

const schema = new Schema(properties, options)

const factory = (connection) => {
  throw new Error("Can't instantiate subdocument")
}

module.exports = { factory }
