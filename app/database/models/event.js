'use strict'

const { Schema } = require('mongoose')

const properties = {
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    required: true
  }
}

const options = {
  id: false,
  safe: true,
  strict: true,
  versionKey: false,
  collection: 'events'
}

const schema = new Schema(options, properties)

const factory = (connection) => {
  return connection.model('Event', schema)
}

module.exports = { factory }
