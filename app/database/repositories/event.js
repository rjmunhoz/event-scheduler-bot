'use strict'

class EventRepository {
  constructor (model) {
    this.$model = model
  }

  async find (id) {
    return this.$model.findOne({ _id: id })
                      .lean()
  }

  async findByOwner (ownerId) {
    return this.$model.find({ 'user._id': ownerId })
                      .lean()
  }

  async findByDate (date) {
    return this.$model.find({ date })
                      .lean()
  }

  async search (filter) {
    return this.$model.find({ name: new RegExp(filter, 'ig') })
                      .lean()
  }
}

module.exports = EventRepository
