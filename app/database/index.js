'use strict'

const mongoose = require('mongoose')

const EventModel = require('./models/event')
const EventStorage = require('./storages/event')
const EventRepository = require('./repositories/event')

const factory = ({ uri }) => {
  const connection = mongoose.createConnection(uri)

  const models = {
    Event: EventModel.factory(connection)
  }

  const repositories = {
    event: new EventRepository(models.Event)
  }

  const storages = { 
    event: new EventStorage(models.Event)
  }

  return { repositories, storages }
}

module.exports = { factory }
