'use strict'

const env = require('sugar-env')

module.exports = {
  telegram: {
    token: env.get('TELEGRAM_API_TOKEN')
  },
  database: {
    uri: env.get('MONGO_URI')
  }
}
